\select@language {portuguese}
\contentsline {section}{\numberline {1}Gen\IeC {\'e}tica de popula\IeC {\c c}\IeC {\~o}es}{1}
\contentsline {subsection}{\numberline {1.1}A gen\IeC {\'e}tica de popula\IeC {\c c}\IeC {\~o}es e suas rela\IeC {\c c}\IeC {\~o}es com a Evolu\IeC {\c c}\IeC {\~a}o}{1}
\contentsline {subsection}{\numberline {1.2}Equil\IeC {\'\i }brio de Hardy-Weinberg}{5}
\contentsline {subsection}{\numberline {1.3}Freq\IeC {\"u}\IeC {\^e}ncias g\IeC {\^e}nicas}{5}
\contentsline {subsection}{\numberline {1.4}Exerc\IeC {\'\i }cios:}{6}
\contentsline {subsection}{\numberline {1.5}Equil\IeC {\'\i }brio (ou lei) de Hardy-Weinberg}{7}
\contentsline {subsection}{\numberline {1.6}Alelos M\IeC {\'u}ltiplos}{9}
\contentsline {subsection}{\numberline {1.7}Estimativa de freq\IeC {\"u}\IeC {\^e}ncias g\IeC {\^e}nicas}{9}
\contentsline {subsection}{\numberline {1.8}Caso de dois alelos com domin\IeC {\^a}ncia}{12}
\contentsline {subsection}{\numberline {1.9}Equil\IeC {\'\i }brio para genes ligados ao sexo}{12}
\contentsline {subsection}{\numberline {1.10}Desvios da pan-mixia: Cruzamentos prefer\IeC {\^e}nciais e endocruzamento}{15}
\contentsline {subsection}{\numberline {1.11}Cruzamento preferencial totalmente negativo}{16}
\contentsline {subsection}{\numberline {1.12}Cruzamento preferencial totalmente positivo}{16}
\contentsline {subsection}{\numberline {1.13}Muta\IeC {\c c}\IeC {\~o}es}{19}
\contentsline {subsection}{\numberline {1.14}Muta\IeC {\c c}\IeC {\~a}o reversa}{20}
\contentsline {subsection}{\numberline {1.15}Deriva gen\IeC {\'e}tica}{21}
\contentsline {subsection}{\numberline {1.16}Sele\IeC {\c c}\IeC {\~a}o Natural}{24}
\contentsline {subsection}{\numberline {1.17}C\IeC {\'a}lculo do valor adaptativo}{24}
\contentsline {subsection}{\numberline {1.18}O modelo geral de sele\IeC {\c c}\IeC {\~a}o}{25}
\contentsline {subsection}{\numberline {1.19}Casos especiais}{26}
\contentsline {subsection}{\numberline {1.20}Equil\IeC {\'\i }brio entre muta\IeC {\c c}\IeC {\~a}o e sele\IeC {\c c}\IeC {\~a}o}{27}
\contentsline {subsection}{\numberline {1.21}Projeto 1. Simula\IeC {\c c}\IeC {\~a}o de deriva gen\IeC {\'e}tica.}{31}
\contentsline {subsection}{\numberline {1.22}Projeto 2. Simula\IeC {\c c}\IeC {\~a}o de sele\IeC {\c c}\IeC {\~a}o natural.}{32}
