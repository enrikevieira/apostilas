\select@language {portuguese}
\contentsline {section}{\numberline {1}Aspectos hist\IeC {\'o}ricos}{1}
\contentsline {subsection}{\numberline {1.1}Galtonistas X Mendelistas}{3}
\contentsline {subsection}{\numberline {1.2}A s\IeC {\'\i }ntese}{4}
\contentsline {subsection}{\numberline {1.3}Os locos de caracteres quantitativos}{5}
\contentsline {section}{\numberline {2}Conceitos b\IeC {\'a}sicos em Gen\IeC {\'e}tica quantitativa}{5}
\contentsline {subsection}{\numberline {2.1}Gen\IeC {\'o}tipos, fen\IeC {\'o}tipos e ambiente}{5}
\contentsline {subsection}{\numberline {2.2}Decomposi\IeC {\c c}\IeC {\~a}o das vari\IeC {\^a}ncias}{6}
\contentsline {subsection}{\numberline {2.3}Epistasia}{9}
\contentsline {subsection}{\numberline {2.4}Resposta \IeC {\`a} sele\IeC {\c c}\IeC {\~a}o}{12}
\contentsline {section}{\numberline {3}Arquitetura gen\IeC {\'e}tica}{12}
\contentsline {subsection}{\numberline {3.1}Arquitetura gen\IeC {\'e}tica do Tipo 1}{12}
\contentsline {subsection}{\numberline {3.2}Arquitetura gen\IeC {\'e}tica do Tipo 2}{13}
\contentsline {subsection}{\numberline {3.3}Arquitetura gen\IeC {\'e}tica do Tipo 3}{13}
\contentsline {subsection}{\numberline {3.4}Arquitetura mista}{13}
\contentsline {subsection}{\numberline {3.5}Locos ou alelos?}{13}
\contentsline {section}{\numberline {4}Marcadores moleculares}{13}
\contentsline {section}{\numberline {5}M\IeC {\'e}todos de an\IeC {\'a}lise de QTLs}{15}
\contentsline {section}{\numberline {6}Perspectivas futuras}{18}
\contentsline {section}{\numberline {7}Bibliografia}{19}
