\select@language {portuguese}
\contentsline {section}{\numberline {1}Informa\IeC {\c c}\IeC {\~o}es de Download}{2}{section.1}
\contentsline {section}{\numberline {2}Primeiros Passos com Maxima}{3}{section.2}
\contentsline {section}{\numberline {3}Iniciando}{4}{section.3}
\contentsline {section}{\numberline {4}Integra\IeC {\c c}\IeC {\~a}o simb\IeC {\'o}lica}{4}{section.4}
\contentsline {section}{\numberline {5}Exemplos Trabalhados:}{7}{section.5}
\contentsline {section}{\numberline {6}Zeros de um polin\IeC {\^o}mio de uma vari\IeC {\'a}vel}{7}{section.6}
\contentsline {section}{\numberline {7}Transforma\IeC {\c c}\IeC {\~o}es Trigonom\IeC {\'e}tricas}{7}{section.7}
\contentsline {section}{\numberline {8}C\IeC {\'a}lculo Passo-a-Passo de uma Decomposi\IeC {\c c}\IeC {\~a}o de Fra\IeC {\c c}\IeC {\~o}es Parciais}{7}{section.8}
\contentsline {section}{\numberline {9}Um cap\IeC {\'\i }tulo para Geometria: Curvas Planas e Espaciais}{7}{section.9}
\contentsline {section}{\numberline {10}Integrais de Linha}{7}{section.10}
\contentsline {section}{\numberline {11}C\IeC {\'a}lculo do Rec\IeC {\'\i }proco de um N\IeC {\'u}mero Irracional}{7}{section.11}
\contentsline {section}{\numberline {12}Aritm\IeC {\'e}tica Polinomial com Restos}{7}{section.12}
\contentsline {section}{\numberline {13}Equa\IeC {\c c}\IeC {\~o}es Diferenciais Ordin\IeC {\'a}rias}{7}{section.13}
\contentsline {section}{\numberline {14}Equa\IeC {\c c}\IeC {\~o}es de Diferen\IeC {\c c}as}{7}{section.14}
\contentsline {section}{\numberline {15}C\IeC {\'a}lculo de Limites}{7}{section.15}
\contentsline {section}{\numberline {16}C\IeC {\'a}lculo de Somat\IeC {\'o}rios Finitos e Infinitos}{7}{section.16}
\contentsline {section}{\numberline {17}Programa\IeC {\c c}\IeC {\~a}o no Maxima}{7}{section.17}
\contentsline {section}{\numberline {18}Acesso de Arquivos a partir do Maxima}{7}{section.18}
\contentsline {section}{\numberline {19}Uso do Lisp}{7}{section.19}
\contentsline {section}{\numberline {20}Fatos sobre Lisp}{7}{section.20}
