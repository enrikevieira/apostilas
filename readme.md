Apostilas
===========

Apostilas montadas a partir do conteudo web disponibilizado. Autor e site do conteudo esta na primeira pagina da apostila.
Este trabalho nao tem fins economicos e objetiva a disponibilizar tais materiais em arquivos unicos e de facil acesso (alem de disponibilizar a codificacao em latex para correcoes e modificacoes).

- Genetica de populacoes
    - acrescentar formulas
    - revisar erros

- Topicos em bioinformatica
    - necessita formatar corretamente
    - revisar erros
    - preencher demais capitulos

- Introducao ao estudo de QTL
    - acrescentar formulas
    - acrescentar figuras
    - acrescentar tabelas
    - necessita formatar corretamente
    - revisar erros
