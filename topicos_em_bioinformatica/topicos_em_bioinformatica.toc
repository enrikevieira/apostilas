\select@language {portuguese}
\contentsline {section}{\numberline {1}T\IeC {\'o}picos em Bioinform\IeC {\'a}tica}{1}
\contentsline {subsection}{\numberline {1.1}Introdu\IeC {\c c}\IeC {\~a}o}{1}
\contentsline {subsection}{\numberline {1.2}Introdu\IeC {\c c}\IeC {\~a}o b\IeC {\'a}sica a estudos gen\IeC {\^o}micos}{2}
\contentsline {subsection}{\numberline {1.3}Bancos de dados de interesse em bioinform\IeC {\'a}tica}{2}
\contentsline {subsubsection}{\numberline {1.3.1}Exercicios suplementares 1 \IeC {\textendash } Seq\IeC {\"u}enciamento e montagem. Bancos de dados}{3}
\contentsline {section}{\numberline {2}Alinhamento de seq\IeC {\"u}encias 1: matrizes de alinhamento. Blast e suas variantes}{7}
\contentsline {section}{\numberline {3}Alinhamento de seq\IeC {\"u}encias 2: FASTA e WU-Blast}{7}
\contentsline {section}{\numberline {4}An\IeC {\'a}lise de seq\IeC {\"u}\IeC {\^e}ncias de biomol\IeC {\'e}culas - Alinhamentos M\IeC {\'u}ltiplos e An\IeC {\'a}lise Filogen\IeC {\'e}tica}{7}
\contentsline {section}{\numberline {5}A inform\IeC {\'a}tica nos estudos de estrutura e fun\IeC {\c c}\IeC {\~a}o de prote\IeC {\'\i }nas}{7}
\contentsline {section}{\numberline {6}Conclus\IeC {\~a}o : Perspectivas da Bioinform\IeC {\'a}tica - Gen\IeC {\^o}mica funcional $>$ prote\IeC {\^o}mica $>$ metabol\IeC {\^o}mica}{7}
